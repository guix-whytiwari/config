(define common-packages
  (list
   ;; cursors
   "bibata-cursor-theme"
   "adw-gtk3-theme"
   ;; fonts
   "font-iosevka"
   "font-google-noto"
   "font-google-noto-emoji"
   "font-google-noto-sans-cjk"
   ;; cli
   "git-minimal"
   "wireplumber"

   "p7zip" "unrar" "unzip"))

(define common-services
  (list (service home-dbus-service-type)
	(service home-pipewire-service-type)))

(define common-config-files
  `(;; git
    ("git/config" ,(local-file "config/git/config"))
    ;; gtk-3
    ("gtk-3.0/settings.ini" ,(local-file "config/gtk-3.0/settings.ini"))
    ;; npm
    ("npm/npmrc" ,(local-file "config/npm/npmrc"))
    ;; wget
    ("wgetrc" ,(local-file "config/wgetrc"))))

;; bash
(define common-aliases
  '(("grep" . "grep --color=auto")
    ("ll" . "ls -l")
    ("ls" . "ls -p --color=auto")))

(define common-bashrc
  (list (local-file "bashrc/bashrc.sh")))

(define common-bash-profile
  (list (local-file "bash-profile/xrdb.sh")
	(local-file "bash-profile/guix-profiles.sh")))

(define common-env-vars
  '(("XDG_BIN_HOME" . "$HOME/.local/bin")
   
    ("CARGO_HOME" . "$XDG_DATA_HOME/cargo")
    ("FFMPEG_DATADIR" . "$XDG_CONFIG_HOME/ffmpeg")
    ("GDBHISTFILE" . "$XDG_DATA_HOME/gdb/history")
    ("GNUPGHOME" . "$XDG_DATA_HOME/gnupg")
    ("GOPATH" . "$XDG_DATA_HOME/go")
    ("GOMODCACHE" . "$XDG_CACHE_HOME/go/mod")
    ("NODE_REPL_HISTORY" . "$XDG_DATA_HOME/node_repl_history")
    ("NPM_CONFIG_USERCONFIG" . "$XDG_CONFIG_HOME/npm/npmrc")
    ("PARALLEL_HOME" . "$XDG_CONFIG_HOME/parallel")
    ("PASSWORD_STORE_DIR" . "$XDG_DATA_HOME/pass")
    ("INPUTRC" . "$XDG_CONFIG_HOME/readline/inputrc")
    ("RLWRAP_HOME" . "$XDG_DATA_HOME/rlwrap")
    ("SCREENRC" . "$XDG_CONFIG_HOME/screen/screenrc")
    ("WGETRC" . "$XDG_CONFIG_HOME/wgetrc")
    ("WINEPREFIX" . "$XDG_DATA_HOME/wineprefixes/default")
    ;; Guix env-vars
    ("GUIX_EXTRA_PROFILES" . "$XDG_DATA_HOME/guix-profiles")))

(define common-path
  '(("PATH" . "$XDG_BIN_HOME:$PATH")))
