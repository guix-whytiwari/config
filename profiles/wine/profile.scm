(use-modules (guix transformations))

(define winetricks-transform
  (options->transformation
    '((with-version . "winetricks=20240105"))))

(concatenate-manifests
 (list
  (packages->manifest
   (list (winetricks-transform
          (specification->package "winetricks"))))

  (specifications->manifest
   '("wine64-staging" "wine-staging"
     "libiconv" "zstd"
     
     "gamescope"))))
