(specifications->manifest
 '("emacs-minimal"

   ;; completion
   "emacs-consult"
   "emacs-embark"
   "emacs-marginalia"
   "emacs-orderless"
   "emacs-vertico"

   ;; exwm
   "emacs-app-launcher"

   ;; mode-line
   "emacs-mood-line"

   ;; guix
   "emacs-buffer-env"
   "emacs-guix"

   ;; org
   "emacs-org-modern"
   ;; prog-comp
   "emacs-cape"
   "emacs-corfu"
   ;; prog-lisp
   "emacs-paredit"
   ;; prog-scheme
   "emacs-geiser-guile"
   ;; prog-go
   "emacs-go-mode"
   ;; prog-rust
   "emacs-rust-mode"

   ;; spell-check
   "emacs-jinx"

   ;; terminal
   "emacs-eat"

   ;; git
   "emacs-magit"

   ;; bluetooth
   "emacs-bluetooth"))
