;; Install mood-line on non-guix managed systems
(unless (or yasht/guix (package-installed-p 'mood-line))
  (package-vc-install "https://gitlab.com/jessieh/mood-line.git"))

(use-package mood-line
  :init (mood-line-mode)
  :config
  (setq mood-line-glyph-alist mood-line-glyphs-unicode))

