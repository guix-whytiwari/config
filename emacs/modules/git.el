;; magit
(use-package magit
  ;; Bing "C-x g" to magit-status
  :bind (("C-x g" . magit-status)))
