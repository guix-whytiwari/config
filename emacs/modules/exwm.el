(use-package exwm
  :init (exwm-enable 1)
  :hook ((exwm-update-class . (lambda ()
				(exwm-workspace-rename-buffer exwm-class-name)))
	 (exwm-update-title . (lambda ()
				(pcase exwm-class-name
				  ("Icecat"   (exwm-workspace-rename-buffer
						       "GNU IceCat"))
				  ("Chromium-browser" (exwm-workspace-rename-buffer
						       "Chromium"))))))
  :config
  (exwm-input-set-key (kbd "C-<XF86AudioLowerVolume>") (lambda () (interactive) (desktop-environment-brightness-decrement)))
  (exwm-input-set-key (kbd "C-<XF86AudioRaiseVolume>") (lambda () (interactive) (desktop-environment-brightness-increment)))
  (exwm-input-set-key (kbd "<XF86KbdBrightnessDown>") (lambda () (interactive) (desktop-environment-keyboard-backlight-decrement)))
  (exwm-input-set-key (kbd "<XF86KbdBrightnessUp>") (lambda () (interactive) (desktop-environment-keyboard-backlight-increment)))
  (exwm-input-set-key (kbd "M-<tab>") 'tab-next)
  (setq exwm-input-simulation-keys
	'(([?\C-b] . [left])
          ([?\C-f] . [right])
          ([?\C-p] . [up])
          ([?\C-n] . [down])
          ([?\C-a] . [home])
          ([?\C-e] . [end])
          ([?\M-v] . [prior])
          ([?\C-v] . [next])
          ([?\C-d] . [delete])
          ([?\C-k] . [S-end delete])
	  ;; Copy & Paste
	  ([?\C-y] . [C-v])
	  ([?\M-w] . [C-c])
	  ;; Find
	  ([?\C-s] . [C-f])
	  )))

;; desktop-environment
(use-package desktop-environment
  :init (desktop-environment-mode 1)
  :bind (("C-<XF86AudioLowerVolume>" . desktop-environment-brightness-decrement)
	 ("C-<XF86AudioRaiseVolume>" . desktop-environment-brightness-increment))
  :config
  (setq desktop-environment-brightness-get-command "light")
  (setq desktop-environment-brightness-set-command "light %s")
  (setq desktop-environment-brightness-get-regexp "^\\([0-9]+\\)")
  (setq desktop-environment-brightness-normal-increment "-A 5")
  (setq desktop-environment-brightness-normal-decrement "-U 5")
  (setq desktop-environment-brightness-small-increment "-A 1")
  (setq desktop-environment-brightness-small-decrement "-U 1")

  (setq desktop-environment-volume-get-command "wpctl get-volume @DEFAULT_AUDIO_SINK@")
  (setq desktop-environment-volume-get-regexp "\\([0-9].[0-9][0-9]\\)")
  (setq desktop-environment-volume-set-command "wpctl set-volume @DEFAULT_AUDIO_SINK@ %s"))
