;; emacs-mac tabs support
(use-package beframe
  :if yasht/macport
  :init (beframe-mode 1)
  :config
  (setq beframe-global-buffers '("*scratch*" "*Messages*" "*Backtrace*")))

(use-package consult
  :if yasht/macport
  :config
  (defvar consult-buffer-sources)
  (declare-function consult--buffer-state "consult")
  (defface beframe-buffer
    '((t :inherit font-lock-string-face))
    "Face for `consult' framed buffers.")

  (defun my-beframe-buffer-names-sorted (&optional frame)
      "Return the list of buffers from `beframe-buffer-names' sorted by visibility.
With optional argument FRAME, return the list of buffers of FRAME."
      (beframe-buffer-names frame :sort #'beframe-buffer-sort-visibility))

  (consult-customize consult--source-buffer :hidden t :default nil)
  (defvar beframe-consult-source
    `( :name     "Workspace Buffers"
       :narrow   ?F
       :category buffer
       ;; :face     beframe-buffer
       :history  beframe-history
       :items    ,#'my-beframe-buffer-names-sorted
       :action   ,#'switch-to-buffer
       :state    ,#'consult--buffer-state))

    (add-to-list 'consult-buffer-sources 'beframe-consult-source))

(use-package emacs
  :if yasht/macport
  :bind (("C-x t 2" . make-frame)
	 ("C-x t 0" . delete-frame))
  :config
  (setq mac-frame-tabbing t))

;; tab-bar
(use-package tab-bar
  :unless yasht/macport
  :init
  (setq tab-bar-show 1)
  (setq tab-bar-close-button-show nil)
  (setq tab-bar-new-button-show nil)
  (tab-bar-mode 1)
  :config
  ;; custom tab name format
  (defun my-tab-bar-name-format (tab i)
    (let ((current-p (eq (car tab) 'current-tab)))
      (propertize
       (concat (if tab-bar-tab-hints (format " %d:" (- i 1)) " ")
               (alist-get 'name tab)
               (or (and tab-bar-close-button-show
			(not (eq tab-bar-close-button-show
				 (if current-p 'non-selected 'selected)))
			tab-bar-close-button)
                   ""))
       'face (funcall tab-bar-tab-face-function tab))))
  (setq tab-bar-tab-name-format-function 'my-tab-bar-name-format)
  (tab-rename "Core" 1))

(unless (package-installed-p 'tabspaces)
  (package-vc-install "https://github.com/mclear-tools/tabspaces.git"))


(use-package tab-bar
  :if yasht/exwm
  :bind (("M-<tab>" . tab-next)))

(use-package tabspaces
  :unless yasht/macport
  :bind (("C-x t 2" . tabspaces-switch-or-create-workspace)
	 ("C-x t 3" . tabspaces-open-or-create-project-and-workspace)
	 ("C-x t 0" . tabspaces-close-workspace))
  :hook (after-init . tabspaces-mode) ;; use this only if you want the minor-mode loaded at startup. 
  :commands (tabspaces-switch-or-create-workspace
             tabspaces-open-or-create-project-and-workspace)
  :custom
  (tabspaces-use-filtered-buffers-as-default t)
  (tabspaces-default-tab "Default")
  (tabspaces-remove-to-default t)
  (tabspaces-include-buffers '("*scratch*"))
  (tabspaces-initialize-project-with-todo t)
  (tabspaces-todo-file-name "project-todo.org"))
  
;; Filter Buffers for Consult-Buffer
(use-package consult
  :unless yasht/macport
  :config
  ;; hide full buffer list (still available with "b" prefix)
  (consult-customize consult--source-buffer :hidden t :default nil)
  ;; set consult-workspace buffer list
  (defvar consult--source-workspace
    (list :name     "Workspace Buffers"
          :narrow   ?w
          :history  'buffer-name-history
          :category 'buffer
          :state    #'consult--buffer-state
          :default  t
          :items    (lambda () (consult--buffer-query
				:predicate #'tabspaces--local-buffer-p
				:sort 'visibility
				:as #'buffer-name)))

    "Set workspace buffer list for consult-buffer.")
  (add-to-list 'consult-buffer-sources 'consult--source-workspace))
