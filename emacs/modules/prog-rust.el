;; enable rust-mode
(use-package rust-mode
  :hook ((rust-mode . (lambda () (setq indent-tabs-mode nil)))
	 (rust-mode . (lambda () (prettify-symbols-mode))))
  :config
  (setq rust-format-on-save t))

;; enable lsp for rust
(use-package eglot
  :hook ((rust-mode . eglot-ensure)))
