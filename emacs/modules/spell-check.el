(use-package jinx
  :init
  ;; Workaround for deadlock on emacs-mac
  ;; Fixed in next version
  (when (eq window-system 'mac)
    (mac-do-applescript
     "use framework \"AppKit\"
      set spellChecker to current application's NSSpellChecker's sharedSpellChecker()"))
  :hook ((org-mode . jinx-mode))
  :bind (("M-$" . jinx-correct)
         ("C-M-$" . jinx-languages)))
