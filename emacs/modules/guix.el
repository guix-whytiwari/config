(use-package buffer-env
  :hook ((comint-mode . hack-dir-local-variables-non-file-buffer)
	 (hack-local-variables . buffer-env-update))
  :config
  (setq buffer-env-script-name "manifest.scm"))
