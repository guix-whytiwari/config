;; emacs
(use-package emacs
  :init
  ;; Unset tab-switch key when using EXWM
  ;; Set C-<tab> to next tab in macport
  (cond (yasht/exwm (global-unset-key (kbd "C-<tab>")))
	;; (yasht/macport (global-set-key (kbd "C-<tab>") 'tab-next))
	)
  :config
  ;; Set backup directory
  (setq backup-directory-alist
	`(("." . ,(concat user-emacs-directory "backups"))))
  ;; Resize frame pixelwise
  (setq frame-resize-pixelwise t)
  ;; Start default frame maximized
  (add-to-list 'default-frame-alist '(fullscreen . maximized))
  ;; Display line numbers
  (global-display-line-numbers-mode 1)
  ;; Set font (Iosevka)
  (set-face-attribute 'default t :font "Iosevka" :height yasht/font-height)
  (set-face-attribute 'default nil :font "Iosevka" :height yasht/font-height)
  ;; Disable menu-bar & tool-bar unless macport
  (unless yasht/macport
      (menu-bar-mode -1)
      (tool-bar-mode -1))
  ;; Disable scroll bar
  (scroll-bar-mode -1)
  ;; Replace yes-no prompts by y-n
  (defalias 'yes-or-no-p 'y-or-n-p)
  ;; Enable modus-themes
  (setq modus-themes-italic-constructs t
	modus-themes-bold-constructs nil)
  ;; Change bg-active to match libadwaita
  (setq modus-themes-vivendi-color-overrides
	'((bg-active . "#303030")))

  (load-theme 'modus-vivendi)

  ;; Hide commands in M-x which do not apply to the current mode.
  ;; Corfu & Vertico commands are hidden, since they are not supposed to be used via M-x.
  (setq read-extended-command-predicate
	#'command-completion-default-include-p))

;; savehist
(use-package savehist
  :init (savehist-mode 1))

;; epa
(use-package epa
  :config
  (setq epa-pinentry-mode 'loopback))

;; auth-source
(use-package auth-source
  :config
  (auth-source-pass-enable))
