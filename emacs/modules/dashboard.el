(use-package org
  :custom
  (org-agenda-files '("~/Documents/Notes/")))

(unless (package-installed-p 'emacs-dashboard)
  (package-vc-install "https://github.com/emacs-dashboard/emacs-dashboard.git"))

(use-package dashboard
  :ensure nil
  :init (dashboard-setup-startup-hook)
  :bind (:map dashboard-mode-map
	      ("n" . dashboard-next-line)
	      ("p" . dashboard-previous-line))
  :config
  (setq dashboard-footer-messages
	'("The one true editor, Emacs!"
	  "Free as free speech, free as free Beer"
	  "Happy coding!"
	  "Welcome to the church of Emacs"))
  (setq dashboard-center-content t))
