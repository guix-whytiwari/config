;; eat
(use-package eat)

(use-package eshell
  :config
  (setq eshell-plain-grep-behavior t)
  (setq eshell-visual-subcommands
	'(("guix" . ("edit" "search" "shell")))))
