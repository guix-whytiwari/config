(defvar yasht/font-height 120
  "Value to use when setting font height")

(defvar yasht/scale 1
  "Value to use when scaling components")

(defvar yasht/macport nil
  "If non-nil, current emacs instance is emacs-mac")

(defvar yasht/exwm nil
  "If non-nil, current emacs instance will be running in exwm")

(defvar yasht/guix nil
  "If non-nil, emacs packages are managed by guix")

(defvar yasht/modules (list "compat-init" "compat-end")
  "List of modules to load")

(defun yasht/load-config-module (module)
  "Load configuration module from user-emacs-directory"
  (load-file (concat user-emacs-directory "/modules/" module ".el")))

(defun yasht/load-host-module (hostname)
  "Load host configuration from user-emacs-directory"
  (load-file (concat user-emacs-directory "/host/" hostname ".el")))

;; Set custom-file
(unless (file-exists-p (concat user-emacs-directory "custom.el"))
  (make-empty-file (concat user-emacs-directory "custom.el")))
(defconst custom-file (expand-file-name "custom.el" user-emacs-directory))

(yasht/load-host-module system-name)
