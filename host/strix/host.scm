(define host-packages
  (list
   ;; browser
   "icecat-minimal"
   "translate-web-pages-icecat"
   "ublock-origin-icecat"

   "ungoogled-chromium"
   "ublock-origin-chromium"
   ;; gui
   "keepassxc"
   "mpv"
   "qbittorrent"
   ;; programming
   "gcc-toolchain"
   "clang"

   "rust"
   "rust:cargo" "rust:rust-src" "rust:tools"))

(define host-env-vars
  '(;; HiDPI
    ("GDK_SCALE" . "2")
    ("GDK_DPI_SCALE" . "0.5")
    ("QT_AUTO_SCREEN_SCALE_FACTOR" . "1")
    ("QT_ENABLE_HIGHDPI_SCALING" . "1")))

(define host-bin-files
  `())

(define host-bash-profile
  (list (local-file "bash-profile/xinput.sh")))

(define host-config-files
  `(;; guix
    ("guix/channels.scm" ,(local-file "config/guix/channels.scm"))))
