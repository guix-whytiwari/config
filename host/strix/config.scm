(use-modules (gnu) (gnu system nss) (guix utils)
             (nongnu packages linux)
             (nongnu system linux-initrd)
             (yasht system linux))
(use-service-modules dbus desktop networking linux xorg)
(use-package-modules certs emacs emacs-xyz linux xorg)

(operating-system
  (kernel (corrupt-linux linux-libre
           #:configs '("CONFIG_MT7921E=m")))
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (host-name "strix")
  (timezone "Asia/Kolkata")
  (locale "en_US.utf8")

  (keyboard-layout (keyboard-layout "us"))

  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets '("/boot/efi"))
                (keyboard-layout keyboard-layout)))

  (mapped-devices
   (list (mapped-device
          (source (uuid "5470b93c-bcb1-4377-bdbb-9c446ae9417b"))
          (target "guix-crypt")
          (type luks-device-mapping))))

  (file-systems (append
                 (list (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/")
                         (type "btrfs")
                         (options "subvol=root,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/boot")
                         (type "btrfs")
                         (options "subvol=boot,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/data")
                         (type "btrfs")
                         (options "subvol=data,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/gnu")
                         (type "btrfs")
                         (options "subvol=gnu,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/home")
                         (type "btrfs")
                         (options "subvol=home,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/var/log")
                         (type "btrfs")
                         (options "subvol=log,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/swap")
                         (type "btrfs")
                         (options "subvol=swap")
                         (dependencies mapped-devices))
                       (file-system
                         (device (uuid "F6E6-53B0" 'fat))
                         (mount-point "/boot/efi")
                         (type "vfat")))
                 %base-file-systems))

  (users (cons (user-account
                (name "yasht")
                (comment "Yash Tiwari")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video")))
               %base-user-accounts))

  (packages (append (list
                     ;; for Emacs
                     emacs emacs-exwm
                     emacs-desktop-environment
                     ;; for Desktop
                     light
                     ;; for Xorg
                     xrdb xinput
                     ;; for HTTPS access
                     nss-certs
                     )
                    %base-packages))

  (services (append (list (service slim-service-type 
                            (slim-configuration
                             (display ":0")
                             (vt "vt7 -dpi 192")))
                          (udev-rules-service 'light light)
                          (udev-rules-service 'pipewire pipewire)
                          (service network-manager-service-type)
                          (service wpa-supplicant-service-type)
                          (service bluetooth-service-type)
                          (service udisks-service-type)
                          (service upower-service-type)
                          (service elogind-service-type)
                          (service dbus-root-service-type)
                          (service modremove-service-type)
                          (service zram-device-service-type
                            (zram-device-configuration
                             (size "16G")
                             (compression-algorithm 'zstd)
                             (priority 100))))
                    %base-services))

  (name-service-switch %mdns-host-lookup-nss))
