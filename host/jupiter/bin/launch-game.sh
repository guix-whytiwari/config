#!/usr/bin/env bash
# Determining WINEPREFIX
MOUNTROOT="/media/$USER"
DEVICE=$(ls -AU "$MOUNTROOT" | head -1)
cd "$MOUNTROOT/$DEVICE"
DEVICE=$(echo "$DEVICE" | tr ' ' - | tr '[:upper:]' '[:lower:]')
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/$DEVICE"
echo Using $WINEPREFIX

if [[ ! -d "$WINEPREFIX" ]]; then
    echo "WINEPREFIX doesn't exist."
    mkdir -p "$WINEPREFIX"
    wineboot
fi

if [[ ! -f "$WINEPREFIX/dxvk" ]]; then
    echo "DXVK not installed. Installing..."
    winetricks dxvk
    touch "$WINEPREFIX/dxvk"
fi

if [[ ! -f "$WINEPREFIX/vkd3d" ]]; then
    echo "VKD3D not installed. Installing..."
    winetricks vkd3d
    touch "$WINEPREFIX/vkd3d"
fi

export WINEESYNC=1
export DXVK_STATE_CACHE_PATH="$XDG_CACHE_HOME/dxvk/$GAME"
mkdir -p "$DXVK_STATE_CACHE_PATH"
export VKD3D_SHADER_CACHE_PATH="$XDG_CACHE_HOME/vkd3d/$GAME"
mkdir -p "$VKD3D_SHADER_CACHE_PATH"

# Launch Game
echo Launching "$GAME"...
cd "$GAME/$BINPATH"

if [[ "$GAMESCOPE_DISABLE" -eq 1 ]]; then
    echo Gamescope Disabled
    wine "$BIN"
else
    gamescope -f -- wine "$BIN"
fi


