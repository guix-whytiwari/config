(define host-packages
  (list ))

(define host-env-vars
  '(;; HiDPI
    ("GDK_SCALE" . "2")
    ("GDK_DPI_SCALE" . "0.5")
    ("QT_AUTO_SCREEN_SCALE_FACTOR" . "1")
    ("QT_ENABLE_HIGHDPI_SCALING" . "1")))

(define host-bin-files
  `((".local/bin/launch-game"
     ,(local-file "bin/launch-game.sh"
		  #:recursive? #t))))

(define host-bash-profile
  (list (local-file "bash-profile/xrandr.sh")
   ;; (local-file "bash-profile/xinput.sh")
   ))

(define host-config-files
  `(("opensd/profiles/sd.profile" ,(local-file "config/opensd/profiles/sd.profile"))))
