(define emacs-config-files
  `( ;; emacs
    ("emacs/early-init.el" ,(local-file "emacs/early-init.el"))
    ("emacs/init.el" ,(local-file "emacs/init.el"))
    ("emacs/modules" ,(local-file "emacs/modules"
				  #:recursive? #t))
    ("emacs/host" ,(local-file "emacs/host"
			       #:recursive? #t))))
