;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
	     (gnu home services)
             (gnu home services desktop)
             (gnu home services shells)
             (whytiwari home pipewire))

(define hostname (gethostname))

(load "common.scm")
(load "emacs.scm")
(load (string-append "host/" hostname "/host.scm"))

(home-environment
 (packages (specifications->packages
	    (append common-packages
		    host-packages)))

 (services
  (append common-services
	  (list (service home-xdg-configuration-files-service-type
			 (append common-config-files
				 emacs-config-files
				 host-config-files))
		(simple-service 'home-xdg-bin-files
				home-files-service-type
				(append host-bin-files))
		(service home-bash-service-type
			 (home-bash-configuration
			  (aliases (append common-aliases))
			  (bashrc (append common-bashrc))
			  (bash-profile (append common-bash-profile
						host-bash-profile))
			  (environment-variables
			   (append common-env-vars
				   host-env-vars
				   common-path))))))))
